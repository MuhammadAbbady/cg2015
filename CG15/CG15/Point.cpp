#include"Point.h"
#include"Buffer.h"
using namespace CG2015::UI;
using namespace CG2015::Utilities;
using namespace sf;
Point::Point(IPoint p /* = Utilities::IPoint() */,unsigned int t /* = 5 */,sf::Color c/* =sf::Color::Red */){
	this->x = p.x;
	this->y = p.y;

	thickness = t;
	color = c;
}

Point::~Point(){

}
std::string Point::getString(){
	std::string res = "";
	res += "p " + std::to_string(getX()) + " " + std::to_string(getY());
	return res;
}
void Point::update(sf::RenderWindow& win){
	if(Mouse::isButtonPressed(sf::Mouse::Right)){
		auto o = Mouse::getPosition(win);
		if(o.x >= getX()-thickness && o.x <= getX()+thickness)
			if(o.y >= getY()-thickness && o.y <= getY()+thickness)
				parent->remove(this);
	}
}
void Point::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	RectangleShape rect = const_cast<RectangleShape&>(_rect);
	rect.setSize(sf::Vector2f(thickness,thickness));
	rect.setOrigin(thickness/2.0f,thickness/2.0f);
	rect.setPosition(getX(),getY());
	rect.setFillColor(color);
	target.draw(rect);
}