#pragma once
#include"../Utilities/ILine.h"
#include"IDrawable.h"
#include<SFML/Graphics.hpp>
#include<string>
namespace CG2015{
	namespace UI{
		class Line:public Utilities::ILine, public IDrawable{
		public:

			/** @brief	The thickness. */
			unsigned int thickness;

			/** @brief	The color. */
			sf::Color color;

			Line(Utilities::ILine p = Utilities::ILine(),unsigned int thickness = 2,sf::Color c=sf::Color::Green);
			~Line();

			std::string getString();
			void update(sf::RenderWindow& win);

		private:

			sf::Vertex _vertices[4];

			/**
			 * @fn	virtual void Line::draw(sf::RenderTarget& target, sf::RenderStates states) const override;
			 *
			 * @brief	Draws.
			 *
			 * @author	Moustapha Saad
			 * @date	31/01/2015
			 *
			 * @param [in,out]	target	Target for the.
			 * @param	states		  	The states.
			 */

			void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		};
	}
}