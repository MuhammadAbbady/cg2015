#include "Line.h"
#include"Buffer.h"
using namespace CG2015::UI;
using namespace CG2015::Utilities;
using namespace sf;
Line::Line(ILine p /* = Utilities::ILine() */,unsigned int t /* = 5 */,sf::Color c/* =sf::Color::Red */){
	this->start = p.start;
	this->end = p.end;

	thickness = t;
	color = c;

	offset = sf::Vector2f(0,0);
}
Line::~Line(){

}
std::string Line::getString(){
	std::string res = "";
	res += "l " + std::to_string(start.getX()) + " " + std::to_string(start.getY()) + " " +std::to_string(end.getX()) + " " + std::to_string(end.getY());
	return res;
}
void Line::update(RenderWindow& win){
	if(Mouse::isButtonPressed(sf::Mouse::Right)){
		auto o = Mouse::getPosition(win);
		if(o.x >= start.getX()-thickness && o.x <= start.getX()+thickness)
			if(o.y >= start.getY()-thickness && o.y <= start.getY()+thickness)
				parent->remove(this);
		if(o.x >= end.getX()-thickness && o.x <= end.getX()+thickness)
			if(o.y >= end.getY()-thickness && o.y <= end.getY()+thickness)
				parent->remove(this);
	}
}

void Line::draw(sf::RenderTarget& target, sf::RenderStates states)const {
	sf::Vector2f ss(start.x,start.y),ee(end.x,end.y);
	sf::Vector2f direction = ee-ss;
	sf::Vector2f unitDirection = direction/std::sqrt(direction.x*direction.x+direction.y*direction.y);
	sf::Vector2f unitPerpendicular(-unitDirection.y,unitDirection.x);

	sf::Vector2f offset = (thickness/2.f)*unitPerpendicular;

	sf::Vertex* vertxs = const_cast<sf::Vertex*>(_vertices);
	vertxs[0].position = ss +offset;
	vertxs[1].position = ee +offset;
	vertxs[2].position = ee -offset;
	vertxs[3].position = ss -offset;

	for(int i=0;i<4;i++)
		vertxs[i].color = color;
	target.draw(vertxs,4,sf::Quads);

	RectangleShape rect1;
	rect1.setSize(sf::Vector2f(thickness*2,thickness*2));
	rect1.setOrigin(thickness,thickness);
	rect1.setPosition(start.getX(),start.getY());
	rect1.setFillColor(color);
	target.draw(rect1);

	RectangleShape rect2;
	rect2.setSize(sf::Vector2f(thickness*2,thickness*2));
	rect2.setOrigin(thickness,thickness);
	rect2.setPosition(end.getX(),end.getY());
	rect2.setFillColor(color);
	target.draw(rect2);

}