#pragma once
#include<SFML/Graphics.hpp>
#include<string>
namespace CG2015{
	namespace UI{
		class Buffer;
		class IDrawable:public sf::Drawable{
		public:
			sf::Vector2f offset;
			Buffer* parent;
			virtual void update(sf::RenderWindow& win){}
			virtual std::string getString(){return "";}
		private:
			void draw(sf::RenderTarget& target, sf::RenderStates states)const {}
		};

	}
}