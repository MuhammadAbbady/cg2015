#pragma once
#include "IDrawable.h"
#include <SFML/Graphics.hpp>
#include <string>
namespace CG2015{
	namespace UI{
		class Button:public IDrawable{
			typedef void(*click)(Button);
		public:
			Button(std::string t = "Button",int x = 0, int y =0, int w=75, int h=25);
			~Button();

			void update(sf::RenderWindow&);
			void setFunction(click);
			void setPosition(int x, int y);
			sf::Vector2f getPosition();
			void setTitle(std::string);
			std::string getTitle();
		private:
			sf::RectangleShape rectangle;
			sf::Text text;
			sf::Color color;
			click clickFunction;
			void draw(sf::RenderTarget& target, sf::RenderStates states)const override;
		};
	}
}