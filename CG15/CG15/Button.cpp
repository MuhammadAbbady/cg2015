#include "Button.h"
#include"WinManager.h"
using namespace CG2015::UI;
using namespace std;
using namespace sf;

Button::Button(string t,int x, int y, int w, int h){
	rectangle = RectangleShape(Vector2f(w,h));
	rectangle.setPosition(x,y);
	//button color
	color = Color(88,214,141,255);
	rectangle.setFillColor(color);
	text = Text(t,*WindowManager::getFont());
	text.setColor(Color(255,255,255,255));
	text.setCharacterSize(16);
	clickFunction = NULL;
}
Button::~Button(){
	clickFunction = NULL;
}
void Button::update(sf::RenderWindow& win){
	text.setPosition(rectangle.getPosition().x+rectangle.getSize().x/2,rectangle.getPosition().y+rectangle.getSize().y/3);
	text.setOrigin(text.getLocalBounds().width/2,text.getLocalBounds().height/2);
	if(Mouse::isButtonPressed(Mouse::Button::Left) == true){
		auto mpos = Mouse::getPosition(win);
		if(mpos.x>=rectangle.getPosition().x && mpos.x<=rectangle.getPosition().x+rectangle.getSize().x)
			if(mpos.y>=rectangle.getPosition().y && mpos.y<=rectangle.getPosition().y+rectangle.getSize().y){
				if(clickFunction)
					clickFunction(*this);
			}
	}
}
void Button::setFunction(click f){
	clickFunction = f;
}

void Button::setPosition(int x, int y){
	rectangle.setPosition(x,y);
}
Vector2f Button::getPosition(){
	return rectangle.getPosition();
}

void Button::setTitle(std::string s){
	text.setString(s);
}
string Button::getTitle(){
	return text.getString();
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states)const{
	target.draw(rectangle);
	target.draw(text);
}