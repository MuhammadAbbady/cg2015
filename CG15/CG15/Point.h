#pragma once
#include"../Utilities/IPoint.h"
#include"IDrawable.h"
#include<SFML/Graphics.hpp>
namespace CG2015{
	namespace UI{
		class Point:public Utilities::IPoint, public IDrawable{
		public:

			/** @brief	The thickness. */
			unsigned int thickness;

			/** @brief	The color. */
			sf::Color color;

			Point(Utilities::IPoint p = Utilities::IPoint(),unsigned int thickness = 5,sf::Color c=sf::Color::Red);
			~Point();

			void update(sf::RenderWindow& win);
			std::string getString();
		private:

			/** @brief	The rectangle. */
			sf::RectangleShape _rect;

			/**
			 * @fn	virtual void Point::draw(sf::RenderTarget& target, sf::RenderStates states) const override;
			 *
			 * @brief	Draws.
			 *
			 * @author	Moustapha Saad
			 * @date	31/01/2015
			 *
			 * @param [in,out]	target	Target for the.
			 * @param	states		  	The states.
			 */

			void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		};
	}
}