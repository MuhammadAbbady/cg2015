#include "../Controller/Controller.h"
#include <iostream>
#include "WinManager.h"
using namespace CG2015::UI;

int main()
{
	std::cout<< Controller::HelloWorld() << std::endl;
	WindowManager man;
	man.start();
	return 0;
}