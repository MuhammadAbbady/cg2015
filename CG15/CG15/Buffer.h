#pragma once
#include <string>
#include <vector>
#include"IDrawable.h"
namespace CG2015{
	namespace UI{
		class WindowManager;
		class Buffer:public IDrawable{
			friend class WindowManager;
		private:

			/** @brief	The drawing data. */
			std::vector<IDrawable*> _data;
		public:


			/**
			 * @fn	Buffer::Buffer(IDrawable* arr, unsigned int arrSize);
			 *
			 * @brief	Constructor.
			 *
			 * @author	Moustapha Saad
			 * @date	05/02/2015
			 *
			 * @param [in,out]	arr	If non-null, the array.
			 * @param	arrSize	   	Size of the array.
			 */

			Buffer(IDrawable* arr[], unsigned int arrSize);
			Buffer();
			~Buffer();

			void setData(IDrawable* arr[], unsigned int arrSize);
			void push(IDrawable*);
			void remove(IDrawable*);
			void update(sf::RenderWindow& win);
			void clear();


			void draw(sf::RenderTarget& target, sf::RenderStates states)const;

		};
	}
}