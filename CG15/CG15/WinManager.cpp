#include"WinManager.h"
#include "Point.h"
#include"Line.h"
#include<sstream>
#include<fstream>
using namespace std;
using namespace CG2015::UI;
using namespace sf;
using namespace CG2015::Utilities;
#define POINTID 1
#define LINEID 2
#define SLIDERID 3
#define BufferID 4
#define SaveID 5
#define LoadID 6
#define DialogID 7
#define AddID 8
#define RemID 9
Font* WindowManager::font = new Font();
WindowManager::WindowManager(unsigned int w/* =800 */,unsigned int h/* =600 */,std::string t/* ="CG2015" */){
	_width = w;
	_height = h;
	_title = t;
	started = false;
	_clearColor = sf::Color::Black;
	font->loadFromFile("arial.ttf");
	_bufferMap["main"] = Buffer();
	drawingBuffer = "main";
	inputOffset = 35;
	loadFlag = false;
	addFlag = false;
	saveFlag = false;
}
WindowManager::~WindowManager(){

}
Font* WindowManager::getFont(){
	return font;
}
std::string WindowManager::getTitle(){
	return _title;
}
unsigned int WindowManager::getWidth(){
	return _width;
}
unsigned int WindowManager::getHeight(){
	return _height;
}
void WindowManager::setWidth(unsigned int val){
	_width = val;
	if(started)
		updateWindowProperties();
}
void WindowManager::setHeight(unsigned int val){
	_height = val;
	if(started)
		updateWindowProperties();
}
void WindowManager::setTitle(std::string val){
	_title = val;
	if(started)
		updateWindowProperties();
}
void WindowManager::updateWindowProperties(){
	_window.setSize(sf::Vector2u(_width,_height));
	_window.setTitle(_title);
}

void WindowManager::setupGUI(tgui::Gui& guii){

	tgui::RadioButton::Ptr rad(guii);
	rad->load("TGUI/widgets/Black.conf");
	rad->setPosition(0,0);
	rad->setSize(20,20);
	rad->setText("Point");
	rad->check();
	point = true;
	rad->bindCallback(tgui::RadioButton::Checked|tgui::RadioButton::Unchecked);
	rad->setCallbackId(POINTID);

	tgui::RadioButton::Ptr rad2(guii);
	rad2->load("TGUI/widgets/Black.conf");
	rad2->setPosition(75,0);
	rad2->setSize(20,20);
	rad2->setText("Line");
	rad2->bindCallback(tgui::RadioButton::Checked|tgui::RadioButton::Unchecked);
	rad2->setCallbackId(LINEID);

	tgui::Slider::Ptr sld(guii);
	sld->load("TGUI/widgets/Black.conf");
	sld->setPosition(150,0);
	sld->setMinimum(1);
	sld->setSize(150,20);
	sld->setVerticalScroll(false);
	sld->setMaximum(10);
	sld->bindCallback(tgui::Slider::ValueChanged);
	sld->setCallbackId(SLIDERID);

	comboBox = tgui::ComboBox::Ptr(guii);
	comboBox->load("TGUI/widgets/Black.conf");
	comboBox->setPosition(325,0);
	comboBox->setSize(100,30);
	comboBox->addItem("main",0);
	comboBox->setSelectedItem("main");
	comboBox->bindCallback(tgui::ComboBox::ItemSelected | tgui::ComboBox::Focused);
	comboBox->setCallbackId(BufferID);

	tgui::Button::Ptr addBu(guii);
	addBu->load("TGUI/widgets/Black.conf");
	addBu->setPosition(430,0);
	addBu->setSize(30,30);
	addBu->setText("+");
	addBu->bindCallback(tgui::Button::LeftMouseClicked);
	addBu->setCallbackId(AddID);

	tgui::Button::Ptr remBu(guii);
	remBu->load("TGUI/widgets/Black.conf");
	remBu->setPosition(500,0);
	remBu->setSize(30,30);
	remBu->setText("-");
	remBu->bindCallback(tgui::Button::LeftMouseClicked);
	remBu->setCallbackId(RemID);


	tgui::Button::Ptr loadButton(guii);
	loadButton->load("TGUI/widgets/Black.conf");
	loadButton->setPosition(570,0);
	loadButton->setSize(60,30);
	loadButton->setText("Load");
	loadButton->bindCallback(tgui::Button::LeftMouseClicked);
	loadButton->setCallbackId(LoadID);

	tgui::Button::Ptr saveButton(guii);
	saveButton->load("TGUI/widgets/Black.conf");
	saveButton->setPosition(640,0);
	saveButton->setSize(60,30);
	saveButton->setText("Save");
	saveButton->bindCallback(tgui::Button::LeftMouseClicked);
	saveButton->setCallbackId(SaveID);

	dialogBox = tgui::EditBox::Ptr(guii);
	dialogBox->load("TGUI/widgets/Black.conf");
	dialogBox->setPosition(-100,-100);
	dialogBox->setSize(300,30);
	dialogBox->setText("Enter File Name");
	dialogBox->bindCallback(tgui::EditBox::ReturnKeyPressed);
	dialogBox->setCallbackId(DialogID);
}
void WindowManager::handleInput(sf::Vector2i mpos){
	if(point){
		Point* p = new Point(IPoint(vec2f(mpos.x,mpos.y)));
		_bufferMap[drawingBuffer].push(p);
	}else{
		Line* l = new Line(ILine(IPoint(vec2f(mpos.x,mpos.y)),IPoint(vec2f(mpos.x,mpos.y))));
		while(Mouse::isButtonPressed(sf::Mouse::Left)){
			cout<<"koko"<<endl;
		}
		auto curMpos = Mouse::getPosition(_window);
		l->end = IPoint(vec2f(curMpos.x,curMpos.y));
		_bufferMap[drawingBuffer].push(l);
	}
}
void WindowManager::load(string fileName){
	_bufferMap[drawingBuffer].clear();
	ifstream file(fileName.c_str());
	if(file.is_open()){
		file>>drawingBuffer;
		_bufferMap[drawingBuffer] = Buffer();
		comboBox->addItem(drawingBuffer);
		char typ = ' ';
		while(file>>typ){
			if(typ == 'p'){
				float x,y;
				file>>x>>y;
				_bufferMap[drawingBuffer].push(new Point(IPoint(vec2f(x,y))));
			}else if(typ =='l'){
				float xs,ys,xe,ye;
				file>>xs>>ys>>xe>>ye;
				_bufferMap[drawingBuffer].push(new Line(ILine(IPoint(vec2f(xs,ys)),
					IPoint(vec2f(xe,ye)))));
			}
		}
	}
	file.close();
}
void WindowManager::save(string fileName){
	ofstream file;
	file.open(fileName.c_str());
	file<<drawingBuffer<<endl;
	for(int i=0;i<_bufferMap[drawingBuffer]._data.size();i++)
		file<<_bufferMap[drawingBuffer]._data[i]->getString()<<endl;
	file.close();
}
void WindowManager::start(){
	_window.create(sf::VideoMode(_width, _height),_title);
	tgui::Gui gui(_window);
	gui.setGlobalFont("arial.ttf");

	setupGUI(gui);


	IDrawable* koko[] = {new Point(IPoint(vec2f(10,10))),
	new Point(IPoint(vec2f(100,100))),
	new Line(ILine(IPoint(vec2f(10,10)),IPoint(vec2f(100,100))))};

	while(_window.isOpen()){
		sf::Event e;
		while(_window.pollEvent(e)){
			if(e.type == sf::Event::Closed)
				_window.close();
			if(e.type == sf::Event::MouseButtonPressed){
				auto mpos = Mouse::getPosition(_window);
				if(mpos.y>inputOffset){
					handleInput(mpos);
					cout<<"point"<<endl;
				}
			}
			gui.handleEvent(e);
		}
		tgui::Callback calb;
		while(gui.pollCallback(calb)){
			if(calb.id == SLIDERID){
				cout<<calb.value<<endl;
			}
			if(calb.id == POINTID){
				point = !point;
			}
			if(calb.id == BufferID){
				if(calb.trigger == tgui::ComboBox::ItemSelected){
					auto f = _bufferMap.find(calb.text.toAnsiString());
					if(f==_bufferMap.end()){
						cout<<"Error Can't find buffer of name = "<<calb.text.toAnsiString()<<endl;
					}else{
						drawingBuffer = calb.text.toAnsiString();
					}
					inputOffset = 35;
				}else if(calb.trigger == tgui::ComboBox::Focused){
					inputOffset=_window.getSize().y;
				}else if(calb.trigger == tgui::ComboBox::MouseLeft){
					inputOffset = 35;
				}
			}
			if(calb.id == LoadID){
				inputOffset = _window.getSize().y;
				auto siz = _window.getSize();
				auto s = dialogBox->getSize();
				dialogBox->setPosition((siz.x/2)-s.x/2,(siz.y/2)-s.y/2);
				loadFlag = true;
			}
			if(calb.id == SaveID){
				inputOffset = _window.getSize().y;
				auto siz = _window.getSize();
				auto s = dialogBox->getSize();
				dialogBox->setPosition((siz.x/2)-s.x/2,(siz.y/2)-s.y/2);
				saveFlag = true;
			}
			if(calb.id == DialogID){
				inputOffset = 35;
				cout<<calb.text.toAnsiString()<<endl;
				dialogBox->setPosition(sf::Vector2f(-100,-100));
				if(loadFlag){
					load(calb.text.toAnsiString());
					loadFlag = false;
				}else if(saveFlag){
					save(calb.text.toAnsiString());
					saveFlag = false;
				}
				if(addFlag){
					_bufferMap[calb.text.toAnsiString()] = Buffer();
					comboBox->addItem(calb.text.toAnsiString());
					comboBox->setSelectedItem(calb.text.toAnsiString());
					drawingBuffer = calb.text.toAnsiString();
					addFlag = false;
				}
				dialogBox->setText("Enter File Name");
			}
			if(calb.id == AddID){
				inputOffset = _window.getSize().y;
				auto siz = _window.getSize();
				auto s = dialogBox->getSize();
				dialogBox->setPosition((siz.x/2)-s.x/2,(siz.y/2)-s.y/2);
				addFlag = true;
			}
			if(calb.id == RemID){
				comboBox->removeItem(comboBox->getSelectedItem());
				_bufferMap.erase(calb.text.toAnsiString());
			}

		}
		_bufferMap[drawingBuffer].update(_window);
		_window.clear(_clearColor);
		_window.draw(_bufferMap[drawingBuffer]);

		gui.draw();
		_window.display();
	}
}
sf::Color WindowManager::getClearColor(){
	return _clearColor;
}
void WindowManager::setClearColor(sf::Color val){
	_clearColor = val;
}