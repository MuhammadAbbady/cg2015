#include "Buffer.h"
#include "Point.h"
#include "Line.h"
using namespace CG2015::UI;
using namespace std;

Buffer::Buffer(IDrawable* arr[], unsigned int arrSize){
	_data.resize(arrSize);
	for(int i=0;i<arrSize;i++){
		arr[i]->parent = this;
		_data[i] = arr[i];
	}
}
Buffer::Buffer(){
	_data = vector<IDrawable*>();
}
Buffer::~Buffer(){
	for(int i=0;i<_data.size();i++)
		delete _data[i];
	_data.clear();
}

void Buffer::setData(IDrawable* arr[], unsigned int arrSize){
	_data.resize(arrSize);
	for(int i=0;i<arrSize;i++){
		arr[i]->parent = this;
		_data[i] = arr[i];
	}
}

void Buffer::draw(sf::RenderTarget& target, sf::RenderStates states)const {
	for(int i=0;i<_data.size();i++){
		target.draw(*_data[i]);
	}
}

void Buffer::push(IDrawable* obj){
	obj->parent = this;
	_data.push_back(obj);
}
void Buffer::remove(IDrawable* obj){
	int ix = -1;
	for(int i=0;i<_data.size();i++)
		if(_data[i] == obj)
			ix = i;
	if(ix!=-1)
		_data.erase(_data.begin()+ix);
}
void Buffer::update(sf::RenderWindow& win){
	for(int i=0;i<_data.size();i++)
		_data[i]->update(win);
}
void Buffer::clear(){
	for(int i=0;i<_data.size();i++)
		delete _data[i];
	_data.clear();
}