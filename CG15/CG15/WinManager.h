#pragma once
#include<SFML/Graphics.hpp>
#include<string>
#include<map>
#include <TGUI/TGUI.hpp>
#include"Buffer.h"
namespace CG2015{
	namespace UI{
		class WindowManager{
		private:

			/** @brief	The font of typing. */
			static sf::Font* font;
			/** @brief	The width. */
			unsigned int _width;

			/** @brief	The height. */
			unsigned int _height;

			/** @brief	The title. */
			std::string _title;

			/** @brief	The window. */
			sf::RenderWindow _window;

			/** @brief	The clear color. */
			sf::Color _clearColor;

			/** @brief	true if started. */
			bool started;

			/** @brief	The graphical user interface. */
			tgui::Gui gui;

			/** @brief	true to point radio button. */
			bool point;

			/** @brief	Buffer for drawing data. */
			std::string drawingBuffer;

			/** @brief	The buffer map. */
			std::map<std::string,Buffer> _bufferMap;

			/** @brief	The dialog box. */
			tgui::EditBox::Ptr dialogBox;
			tgui::ComboBox::Ptr comboBox;

			/** @brief	The input offset of mouse click adding objects. */
			float inputOffset;

			bool loadFlag,addFlag,saveFlag;

			void updateWindowProperties();
			void setupGUI(tgui::Gui&);
			void handleInput(sf::Vector2i);
			void load(std::string);
			void save(std::string);
		public:
			WindowManager(unsigned int w=800,unsigned int h=600,std::string t="CG2015");

			/**
			 * @fn	void WindowManager::start();
			 *
			 * @brief	Starts this window.
			 *
			 * @author	Moustapha Saad
			 * @date	31/01/2015
			 */

			void start();

			~WindowManager();

			static sf::Font* getFont();

			unsigned int getWidth();
			unsigned int getHeight();
			std::string getTitle();
			sf::Color getClearColor();
			void setClearColor(sf::Color val);
			void setWidth(unsigned int val);
			void setHeight(unsigned int val);
			void setTitle(std::string val);

		};
	}
}