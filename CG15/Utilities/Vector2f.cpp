#include "Vector2f.h"
#include <cmath>
using namespace CG2015::Utilities;
using namespace std;

Vector2f::Vector2f(float _x/* =0 */, float _y/* =0 */){
	x = _x;
	y = _y;
}

float Vector2f::Dot(const Vector2f& r){
	float res = x*r.x + y*r.y;
	return res;
}

float Vector2f::Cross(const Vector2f& r){
	float res = x * r.y - y*r.x;
	return res;
}

float Vector2f::Max(){
	return x > y ? x : y;
}

float Vector2f::Min(){
	return x < y ? x : y;
}

float Vector2f::LengthSquared(){
	return this->Dot(*this);
}

float Vector2f::Length(){
	return sqrt(LengthSquared());
}

Vector2f Vector2f::Normalized(){
	return *this/Length();
}

Vector2f Vector2f::operator+(const Vector2f& r){
	return Vector2f(x+r.x, y+r.y);
}

Vector2f Vector2f::operator-(const Vector2f& r){
	return Vector2f(x-r.x, y-r.y);
}

Vector2f Vector2f::operator*(const float r){
	return Vector2f(x*r,y*r);
}

Vector2f Vector2f::operator/(const float r){
	return Vector2f(x/r,y/r);
}

Vector2f Vector2f::operator*=(const float r){
	return *this*r;
}

Vector2f Vector2f::operator+=(const Vector2f& r){
	return *this+r;
}

Vector2f Vector2f::operator/=(const float r){
	return *this/r;
}

Vector2f Vector2f::operator-=(const Vector2f& r){
	return *this-r;
}

bool Vector2f::operator==(const Vector2f& r){
	bool res = x == r.x && y == r.y ? true : false;
	return res;
}

bool Vector2f::operator!=(const Vector2f& r){
	return !(*this == r);
}