#pragma once
namespace CG2015{
	namespace Utilities{
		template<class Prop, class Obj>
		class Property{

			/**
			 * @typedef	Prop (Obj::* _get)() const
			 *
			 * @brief	Defines an alias representing the Get function pointer.
			 */

			typedef Prop (Obj::* _get)()const;

			/**
			 * @typedef	void (Obj::* _set)(Prop)
			 *
			 * @brief	Defines an alias representing the Set function pointer.
			 */

			typedef void (Obj::* _set)(Prop);


			/** @brief	The object instance. */
			Obj& obj_instance;

			/** @brief	Pointer to get function. */
			_get get;


			/** @brief	Pointer to Set Function. */
			_set set;

			Prop getVal() const{
				return (obj_instance.*get)();
			}
			void setVal(const Prop& val){
				(obj_instance.*set)(val);
			}
		public:

			/**
			 * @fn	Property::Property(Obj& o, _get g, _set s)
			 *
			 * @brief	Constructor of the property.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @param [in,out]	o	The Obj&amp; that has the Property.
			 * @param	g		 	The _get pointer to the get function.
			 * @param	s		 	The _set pointer to the set function.
			 */

			Property(Obj& o, _get g, _set s)
				:obj_instance(o), get(g), set(s){
			}

			/**
			 * @fn	operator Property::Prop()
			 *
			 * @brief	Cast that class converts it to the given Type.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @return	The return of the get function.
			 */

			operator Prop() const{
				return (obj_instance.*get)();
			}

			/**
			 * @fn	void Property::operator=(Prop val)
			 *
			 * @brief	Assignment operator.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @param	val	The value that will be assigned to the property.
			 */

			void operator =(const Prop& val){
				(obj_instance.*set)(val);
			}
			void operator =(const Property<Prop,Obj>& val){
				setVal(val.getVal());
			}

			Property<Prop,Obj> operator+=(const Prop& val){
				setVal(getVal()+val);
				return *this;
			}
			Property<Prop,Obj> operator-=(const Prop& val){
				setVal(getVal()-val);
				return *this;
			}
			Property<Prop,Obj> operator*=(const Prop& val){
				setVal(getVal()*val);
				return *this;
			}
			Property<Prop,Obj> operator/=(const Prop& val){
				setVal(getVal()/val);
				return *this;
			}

			Property<Prop,Obj> operator+=(const Property<Prop,Obj>& val){
				setVal(getVal()+val.getVal());
				return *this;
			}
			Property<Prop,Obj> operator-=(const Property<Prop,Obj>& val){
				setVal(getVal()-val.getVal());
				return *this;
			}
			Property<Prop,Obj> operator*=(const Property<Prop,Obj>& val){
				setVal(getVal()*val.getVal());
				return *this;
			}
			Property<Prop,Obj> operator/=(const Property<Prop,Obj>& val){
				setVal(getVal()/val.getVal());
				return *this;
			}



			/*
			bool operator==(Prop val){
				return getVal()==val;
			}
			bool operator==(const Property<Prop,Obj>& val){
				return getVal() == val.getVal();
			}
			bool operator!=(Prop val){
				return getVal()!=val;
			}
			bool operator!=(const Property<Prop,Obj>& val){
				return getVal()!=val.getVal();
			}

			friend std::ostream& operator<<(std::ostream &os, const Property<Prop,Obj>& p){
				os<<p.getVal();
				return os;
			}*/


		};
	}
}