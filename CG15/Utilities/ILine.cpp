#include"ILine.h"
using namespace CG2015::Utilities;
ILine::ILine(IPoint _start /* = IPoint() */, IPoint _end/* =IPoint() */){
	start = _start;
	end = _end;
	handleOrder();
}

bool ILine::isVertical(){
	return start.x == end.y;
}

void ILine::handleOrder(){
	IPoint startPoint = start, endPoint = end;
	start	= startPoint < endPoint ? startPoint : endPoint;
	end		= startPoint > endPoint ? startPoint : endPoint;
}

bool ILine::operator<(const ILine& l){
	if(start == l.start)
		return end < l.end;
	return start < l.start;
}