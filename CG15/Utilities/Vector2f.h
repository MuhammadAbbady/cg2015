#pragma once
namespace CG2015{
	namespace Utilities{
		class Vector2fTest;
		class Vector2f{
			friend class CG2015::Utilities::Vector2fTest;
		public:

			/**
			 * @property	float _x, _y
			 *
			 * @brief	Represents the x and y coordinates of the vector.
			 *
			 */

			float x, y;


			Vector2f(float _x=0, float _y=0);

			/**
			 * @fn	Vector2f Vector2f::Dot(const Vector2f r);
			 *
			 * @brief	Performs a Dot Product of this vector and r.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @param	r	The const Vector2f to process.
			 *
			 * @return	A Float.
			 */

			float Dot(const Vector2f& r);

			/**
			 * @fn	float Vector2f::Cross(const Vector2f& r);
			 *
			 * @brief	Cross the given r.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @param	r	The const Vector2f&amp; to process.
			 *
			 * @return	A float.
			 */

			float Cross(const Vector2f& r);
			/**
			 * @fn	float Vector2f::Max();
			 *
			 * @brief	Determines the maximum value of this vector.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @return	The maximum value.
			 */

			float Max();

			/**
			 * @fn	float Vector2f::Min();
			 *
			 * @brief	Determines the minimum value.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @return	The minimum value.
			 */

			float Min();

			/**
			 * @fn	float Vector2f::LengthSquared();
			 *
			 * @brief	Length squared of this 2D Vector.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @return	A float represents the length squared.
			 */

			float LengthSquared();

			/**
			 * @fn	float Vector2f::Length();
			 *
			 * @brief	Gets the length.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @return	A float.
			 */

			float Length();

			/**
			 * @fn	Vector2f Vector2f::Normalized();
			 *
			 * @brief	Gets the normalized vector of this vector.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @return	A Vector2f.
			 */

			Vector2f Normalized();

			Vector2f operator+(const Vector2f& r);

			Vector2f operator-(const Vector2f& r);

			Vector2f operator*(const float r);

			Vector2f operator/(const float r);

			Vector2f operator+=(const Vector2f& r);

			Vector2f operator-=(const Vector2f& r);

			Vector2f operator*=(const float r);

			Vector2f operator/=(const float r);

			bool operator==(const Vector2f& r);

			bool operator!=(const Vector2f& r);

		};
		typedef Vector2f vec2f;
	}
}