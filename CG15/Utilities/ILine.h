#pragma once
#include "IPoint.h"
namespace CG2015{
	namespace Utilities{
		class ILine
		{
		public:
			ILine(IPoint _start = IPoint(), IPoint _end=IPoint());

			bool operator == (const ILine& l);

			/**
			 * @fn	bool ILine::isVertical();
			 *
			 * @brief	Query if this line is vertical.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 *
			 * @return	true if vertical, false if not.
			 */

			bool isVertical();

			/**
			 * @fn	void ILine::handleOrder();
			 *
			 * @brief	Handles the order of the start and end point.
			 *
			 * @author	Moustapha Saad
			 * @date	26/01/2015
			 */

			void handleOrder();

			bool operator <(const ILine& l);

			friend std::ostream& operator <<(std::ostream& os, const ILine& l){
				os<< l.start <<" "<< l.end;
				return os;
			}
			IPoint start, end;
		};
	}
}