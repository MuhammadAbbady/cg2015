#pragma once
#include "Vector2f.h"
#include <iostream>
#include "Property.h"
namespace CG2015{
	namespace Utilities{
		class IPoint
		{
		protected:
			/** @brief	The coordinate of the point in 2D space. */
			vec2f _p;
		public:


			/** @brief	The Property&lt;float,IPoint&gt; to process. */
			Property<float, IPoint> x;

			/** @brief	The Property&lt;float,IPoint&gt; to process. */
			Property<float, IPoint> y;

			IPoint(vec2f p = vec2f());
			IPoint(const IPoint& pp);

			float getX()const;
			float getY()const;

			void setX(float val);
			void setY(float val);

			bool operator<(const IPoint& p);

			bool operator>(const IPoint& p);

			IPoint operator+(const IPoint& p);

			IPoint operator-(const IPoint& p);

			IPoint operator*(const float p);

			IPoint operator/(const float p);

			bool operator==(const IPoint& p);

			bool operator!=(const IPoint& p);

			void operator=(const IPoint&p);

			friend std::ostream& operator<<(std::ostream &os, const IPoint& p){
				os << p._p.x << " " <<  p._p.y;
				return os;
			}

		};
	}
}