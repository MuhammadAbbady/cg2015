#include "IPoint.h"
#include <cmath>
using namespace CG2015::Utilities;

IPoint::IPoint(vec2f p /* = vec2f() */)
		:x(*this, &IPoint::getX,&IPoint::setX),
		 y(*this, &IPoint::getY,&IPoint::setY)
{
	_p = p;
}
IPoint::IPoint(const IPoint& pp)
	:x(*this, &IPoint::getX,&IPoint::setX),
	y(*this, &IPoint::getY,&IPoint::setY)
{
	_p.x = pp._p.x;
	_p.y = pp._p.y;
}

bool IPoint::operator<(const IPoint& v){
	if(fabs(_p.x - v._p.x) <= 1e-3)
		return _p.y < v._p.y;
	return _p.x < v._p.x;
}

bool IPoint::operator>(const IPoint& v){
	if(fabs(_p.x - v._p.x) <= 1e-3)
		return _p.y > v._p.y;
	return _p.x > v._p.x;
}

IPoint IPoint::operator+(const IPoint& p){
	return IPoint(_p+p._p);
}

IPoint IPoint::operator-(const IPoint& p){
	return IPoint(_p-p._p);
}

IPoint IPoint::operator*(const float p){
	return IPoint(_p*p);
}

IPoint IPoint::operator/(const float p){
	return IPoint(_p/p);
}

bool IPoint::operator==(const IPoint &v){
	return fabs(v._p.x-_p.x)<1e-3 && fabs(v._p.y-_p.y)<1e-3;
}
bool IPoint::operator!=(const IPoint& v){
	return !(*this == v);
}

void IPoint::operator=(const IPoint& p){
	_p.x = p._p.x;
	_p.y = p._p.y;
}

float IPoint::getX()const {
	return _p.x;
}

float IPoint::getY()const{
	return _p.y;
}

void IPoint::setX(float val){
	_p.x = val;
}

void IPoint::setY(float val){
	_p.y = val;
}