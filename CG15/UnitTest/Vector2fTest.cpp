#include "stdafx.h"
#include "../Utilities/Vector2f.h"
#include <CppUnitTest.h>
#include <exception>
using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace CG2015::Utilities;

BEGIN_TEST_MODULE_ATTRIBUTE()
	TEST_MODULE_ATTRIBUTE(L"Project", L"Utilities")
	TEST_MODULE_ATTRIBUTE(L"Owner", L"Moustafa")
	END_TEST_MODULE_ATTRIBUTE()

TEST_MODULE_INITIALIZE(ModuleStartup){
	Logger::WriteMessage(L"The test is starting ...");
}

TEST_MODULE_CLEANUP(ModuleFinalize){
	Logger::WriteMessage(L"Finalizing the test.");
}

TEST_CLASS(CG2015::Utilities::Vector2fTest){
public:
	Vector2fTest(){
		Logger::WriteMessage(L"Constructor of vec2f test");
	}

	TEST_CLASS_INITIALIZE(ClassInitialize)
	{
		Logger::WriteMessage(L"Initializing the class");
	}
	//optional finalization method if needed
	TEST_CLASS_CLEANUP(ClassFinalize)
	{
		Logger::WriteMessage(L"Finalizing the class");
	}
	
	BEGIN_TEST_METHOD_ATTRIBUTE(TestInitialData)
		TEST_OWNER(L"Moustapha")
		TEST_DESCRIPTION(L"test the class")
		END_TEST_METHOD_ATTRIBUTE()

	TEST_METHOD(TestInitialData){
		Vector2f v;
		Assert::AreEqual(v.x,0.0f);
		Assert::AreEqual(v.y,0.0f);
	}
	TEST_METHOD(TestDot){
		Vector2f v1(9.13,6.134),v2(67.4,23);
		float r = v1.Dot(v2);
		Assert::AreEqual(r,756.444f);
	}
	TEST_METHOD(TestCross){
		Vector2f v1(9.13,6.134),v2(67.4,23);
		float r = v1.Cross(v2);
		Assert::AreEqual(r,-203.442f);
	}
	TEST_METHOD(TestMax){
		Vector2f v(8,12);
		Assert::AreEqual(v.Max(),12.0f);
	}
	TEST_METHOD(TestMin){
		Vector2f v(8,12);
		Assert::AreEqual(v.Min(),8.0f);
	}

	TEST_METHOD(TestLengthSquared){
		Vector2f v(8,12);
		Assert::AreEqual(v.LengthSquared(),4.0f);
	}

	TEST_METHOD(TestLength){
		Vector2f v(8,12);
		Assert::AreEqual(v.Length(),14.4222f);
	}
	TEST_METHOD(TestNormalized){
		Vector2f v(8,12);
		v = v.Normalized();
		Assert::AreEqual(v.x,0.5547f);
		Assert::AreEqual(v.y,0.83205f);
	}

};