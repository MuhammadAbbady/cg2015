#include "../AlgorithmPackage/AlgorithmFactory.h"
#include <string>
class Controller
{
public:
	static std::string HelloWorld()
	{
		return AlgorithmFactory::HelloWorld() + " & Controller";
	}
};