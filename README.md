#Computational Geometry Library#

This is a Computational Geometry Library implemented mainly to be used in educational purposes.

### What is this repository for? ###

* Implementing a well-designed, documented CG library to be used in educational purposes that enables students to spend their time designing a good CG algorithms rather than designing the library itself and helps the TA's also in testing the students' purposed solutions efficiently.


### Currently Working In ###

* Set the basic structure and design of the library

### To-DO feature list ###
* 

### Repo owner or admin ###
* Mohamed Sharaf: (mohammedsharaf.1992@gmail.com)